// 
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json()
.then(perResult => showPosts(perResult)));

const showPosts = (posts) => {
	let postEntries = '';

	posts.forEach((post) => {
		// div >> post-1
		// h3 >> post-title-1
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>			
			</div>
		`
	})
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

document.querySelector('#form-add-post').addEventListener('submit', (event) => {

	event.preventDefault();

    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
          title: document.querySelector('#txt-title').value,
        	body: document.querySelector('#txt-body').value,
          userId: 101
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
    })
    .then(response => response.json())
    .then(result => {
        console.log(result);
        alert('Successfully added.');

        document.querySelector('#txt-title').value = null
        document.querySelector('#txt-body').value = null
    })

})

const editPost = id => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML;
    let body = document.querySelector(`#post-body-${id}`).innerHTML;

    document.querySelector(`#txt-edit-id`).value = id;
    document.querySelector(`#txt-edit-title`).value = title;
    document.querySelector(`#txt-edit-body`).value = body;
    document.querySelector(`#btn-submit-update`).removeAttribute('disabled');
}

document.querySelector('#form-edit-post').addEventListener('submit', ev =>{
    ev.preventDefault();

    fetch("https://jsonplaceholder.typicode.com/posts/1", {
        method: "PUT",
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
        	body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
    })
    .then(response => response.json())
    .then(async result => {
        console.log(result);
        alert('Successfully updated.');

        // await fetch('https://jsonplaceholder.typicode.com/posts')
        // .then((response) => response.json()
        // .then(perResult => showPosts(perResult)));

        document.querySelector('#txt-edit-id').value = null;
        document.querySelector('#txt-edit-title').value = null;
        document.querySelector('#txt-edit-body').value = null;
        document.querySelector(`#btn-submit-update`).disabled = true;
    })
})

const deletePost = id => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
        method: "DELETE",
    })
    .then(response => response.json())
    .then(result => {
        alert('Successfully deleted.');
        document.querySelector(`#post-${id}`).remove();
    })
}